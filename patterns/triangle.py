#!/usr/local/bin/python3

i = 1

limit_str = input("How many lines do you want the triangle to be? (Less than 10): ")
try:
	limit = int(limit_str)
	if limit >= 10:
		print("Cannot use more than 9! Using default of 9")
		limit = 9
except:
	print("That's not a valid number. Using default of 9")
	limit = 9

while i <= limit:
	j = 1
	while j <= i:
		print(j, end="")
		j += 1
	i += 1
	print()