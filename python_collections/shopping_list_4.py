#!/usr/local/bin/python3

shopping_list = []


def remove_item(position):
	try:
		index = int(position) - 1
		item = shopping_list.pop(index)
	except ValueError:
		print("Could not convert the given value to a valid index.")
	except IndexError:
		print("Index provided is not valid!")
	
	print("Removed {}".format(item))

def show_help():
	print("Add new items to the list separated using commas.")
	print("Use HELP to get this help message, SHOW to show the list, "
		  "REMOVE to delete an item, and DONE to exit the program.")
	print()
	

def show_list():
	count = 1
	for item in shopping_list:
		print("{}. {}".format(count, item))
		count += 1
		

print("Tell me what items you need to shop for.\n")
show_help()


def add_items(item_list, index):
	for item in item_list:
		shopping_list.insert(index, item.strip())


while True:
	new_items = input('> ')
	new_list = new_items.split(',')
	
	size = len(shopping_list)
	
	if new_items == 'DONE':
		print("\nHere's your list: ")
		show_list()
		break
	elif new_items == 'SHOW':
		show_list()
		continue
	elif new_items == 'HELP':
		show_help()
		continue
	elif new_items == 'REMOVE':
		show_list()
		index = input("Which item to remove? Give me a number. ")
		remove_item(index)
		continue
	else:
		spot = input("Should I add the list at a certain spot in the list? "
			  "List currently has {} items. ".format(size))
		if spot:
			try:
				index = int(spot) - 1
				add_items(new_list, index)
			except:
				print("Not a legal index! Using end of the list as default.")
				add_items(new_list, size)
				continue
		else:
			add_items(new_list, size)