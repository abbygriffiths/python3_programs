#!/usr/local/bin/python3

def main():
    #Creating a dict is done using {key:value}
    my_dict = {"name": "Abby",
               "job": "Programmer"}

    #Deleting keys is done using del
    del my_dict['job']
    print(my_dict)

    #Adding keys simply involves putting it there like dict['new key'] = value
    my_dict['age'] = 15
    print(my_dict)

    #Changing values is the same
    my_dict['age'] = 16
    print(my_dict)

    #Using the update method is easier
    my_dict.update({'job': 'Programmer', 'age': 15, 'state': 'Maharashtra', 'name': 'Abby Griffiths'})
    print(my_dict)

    #Dictionaries can also be used to format strings.
    my_string = "My name is {name} and I am from {state}."
    print(my_string.format(**my_dict))
    
    #Iterating over dicts is done like for key, value in dict
    for key, value in my_dict.items():
        print('{}: {}'.format(key, value))


if __name__ == "__main__":
    main()