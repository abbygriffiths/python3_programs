#!/usr/bin/env python3

import random, sys, os

player, monster, door = 0, 0, 0

CELLS = ((0, 0), (0, 1), (0, 2),
		 (1, 0), (1, 1), (1, 2),
		 (2, 0), (2, 1), (2, 2))

visited_rooms = set()


def get_random_locations():
	#TODO: Monster, Door, Player = random, random, random	
	monster = random.choice(CELLS)
	start = random.choice(CELLS)
	door = random.choice(CELLS)
	
	if monster == start or monster == door or start == door:
		return get_random_locations()
	
	return (start, monster, door)


def show_dungeon():
	tile = "|{}"
	print(" _  _  _  ")
	for index, room in enumerate(CELLS):
		if index in [0, 1, 3, 4, 6, 7]:
			if room == player and player == door:
				print(tile.format("D"), end = " ")
			elif room == monster:
				print(tile.format("M"), end = " ")
			elif room == player:
				print(tile.format("X"), end = " ")
			elif room in visited_rooms:
				print(tile.format("*"), end = " ")
			else:
				print(tile.format("_"), end = ' ')
		else:
			if room == player and player == door:
				print(tile.format("D|"))
			elif room == monster:
				print(tile.format("M|"))
			elif room == player:
				print(tile.format("X|"))
			elif room in visited_rooms:
				print(tile.format("*|"))
			else:
				print(tile.format('_|'))
			

def move_player(person, move):
	#get player's current location.
	(x, y) = person
	
	if move == 'LEFT':
		y -= 1	
	elif move == 'RIGHT':
		y += 1
	elif move == 'UP':
		x -= 1
	elif move == 'DOWN':
		x += 1
	
	global visited_rooms, player
	if person == player:
		visited_rooms.add(person)
	
	return (x, y)


def get_available_moves(player):
	moves = ['LEFT', 'RIGHT', 'UP', 'DOWN']
	(x, y) = player
	
	if x == 0:
		moves.remove('UP')
		
	if y == 0:
		moves.remove('LEFT')
		
	if x == 2:
		moves.remove('DOWN')
		
	if y == 2:
		moves.remove('RIGHT')
	
	return moves


def show_help():
	print("The small grid above is the dungeon map.")
	print("You are represented on the map by the 'X' symbol.")
	print("The 'M' on the map represents the monster. It moves around randomly. Avoid this monster.")
	print("Rooms you have not been in are shown using '-'. When you visit a room, it turns into '*'")
	print('Enter QUIT to quit the game or VISITED to check rooms you have been before.')
	print("Or enter HELP to show this help.")


player, monster, door = get_random_locations()


def main():
	print('Welcome to the dungeon.')
	
	global player, monster
	while True:
		os.system('clear')
		print("You're currently in room {}.".format(player)) #TODO: Format the specifier with current position of the player
		
		show_dungeon()
		
		available_moves = get_available_moves(player)
		print("You can move {}".format(available_moves))
		
		print('Enter QUIT to quit the game or VISITED to check rooms you have been before.')
		
		move = input('-> ').upper()
		if move == 'QUIT':
			break
		elif move == 'VISITED':
			for room in visited_rooms:
				print(room)
			continue
		elif move == 'HELP':
			show_help()
			continue
		
		monster_moves = get_available_moves(monster)
		monster = move_player(monster, random.choice(monster_moves))
		
		if move in available_moves:
			player = move_player(player, move)
			if player == door:
				os.system('clear')
				print("You found the door to exit this dungeon! You won the game!!!")
				show_dungeon()
				break
			elif player == monster:
				os.system('clear')
				print("Their was a monster in this room. You lose...")
				show_dungeon()
				break
			else:
				continue
		else:
			print("Invalid move!", file = sys.stderr)
			
	
if __name__ == '__main__':
	show_help()
	main()