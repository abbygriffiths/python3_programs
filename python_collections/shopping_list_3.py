#!/usr/local/bin/python3

shopping_list = []


def show_help():
	print("\nAdd new items to the list separated using commas.")
	print("Use HELP to get this help message, SHOW to show the list, or DONE to exit the program.")
	print()
	

def show_list():
	count = 1
	for item in shopping_list:
		print("{}. {}".format(count, item))
		count += 1
		

print("\nTell me what items you need to shop for.")
show_help()


def add_items(item_list, index):
	for item in item_list:
		shopping_list.insert(index, item.strip())


while True:
	new_items = input('> ')
	new_list = new_items.split(',')
	
	size = len(shopping_list)
	
	if new_items == 'DONE':
		print("\nHere's your list: ")
		show_list()
		break
	elif new_items == 'SHOW':
		show_list()
		continue
	elif new_items == 'HELP':
		show_help()
		continue
	else:
		spot = input("Should I add the list at a certain spot in the list? "
			  "List currently has {} items. ".format(size))
		if spot:
			try:
				index = int(spot) - 1
				add_items(new_list, index)
			except:
				print("Not a legal index! Using end of the list as default.")
				add_items(new_list, size)
				continue
		else:
			add_items(new_list, size)