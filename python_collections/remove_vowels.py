#!/usr/local/bin/python3

words = input("Enter a sentence to remove vowels from it. ").split()
vowels = list('aeiou')
output = []

for state in words:
	word_list = list(words.lower())
	
	for vowel in vowels:
		while True:
			try:
				word_list.remove(vowel)
			except:
				break
	output.append(' '.join(word_list).capitalize())
	
print(str(output)) 