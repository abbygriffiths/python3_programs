#!/usr/local/bin/python3

def main():
	print('Hello')
	
	a = 10
	b = 20
	
	print('a: {}, b: {}'.format(a, b))
	
	a, b = b, a
	print('a: {}, b: {}'.format(a, b))
	
	
if __name__ == '__main__':
	main()