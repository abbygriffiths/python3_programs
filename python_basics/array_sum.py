#!/usr/local/bin/python3

def sum(array):
  total = 0
  
  for item in array:
    total += item
    
  return total
  
array = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
print("The sum of numbers between {} and {} is {}".format(array[0], array[len(array) - 1], sum(array)))