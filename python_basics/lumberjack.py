#!/usr/local/bin/python3

name = input("What's your name? ") 
if name == 'Abby':
  print("{} is a lumberjack and they are okay!".format(name))
else:
  print("{} sleeps all night and {} works all day!".format(name, name))  