#!/usr/local/bin/python3
#coding=utf-8

from random import randint

random_number = randint(1, 10)
guessed_numbers = []
max_attempts = 5
tries = 0

print("Guess the number between 1 and 10. You have {} chances.".format(max_attempts))
while len(guessed_numbers) < max_attempts:
  guess_number = 0
  try:
    guess_number = int(input("Guess the number. {} tries done. ".format(tries)))
  except:
    print("That is not a whole number!")
    continue
  
  if guess_number <= 0 or guess_number >= 11:
    print("That is not a number between 1 and 10!")
    continue
  elif guess_number in guessed_numbers:
    print("You've already guessed this number before.")
    continue
  
  guessed_numbers.append(guess_number)
  tries += 1
  
  if guess_number < random_number:
    print("Your guess is lower than the number. Try again. {} tries done.".format(tries))
    continue
  elif guess_number > random_number:
    print("Your guess is higher than the number. Try again.".format(tries))
    continue
  else:
    break
  
if random_number in guessed_numbers:
  print("You guessed the number in {} tries!".format(tries))
else:
  print("You could not guess the number. It was {}.".format(random_number))    