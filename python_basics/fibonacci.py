#!/usr/local/bin/python3

limit_str = input("How many Fibonacci elements to print? ")
i = 0
fib = [0, 1]

try:
    limit = int(limit_str)
except:
    print("That is not a whole number! Using default value of 10.")
    limit = 10

while len(fib) < limit:
    fib.append(fib[i] + fib[i + 1])
    i += 1
    
for number in fib:
    print(number)