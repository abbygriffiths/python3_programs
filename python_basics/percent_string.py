#!/usr/local/bin/python3

user_string = input("What string do you want to use? ")
user_number = input("What's your number? ")
try:
  number = int(user_number)
except:
  number = float(user_number)
  
if len(user_string) < number:
  print("The number you entered is too big for this string!")
elif not '.' in user_number:
  print(user_string[number])
else:
  ratio = round(len(user_string) * number)
  print(user_string[ratio])