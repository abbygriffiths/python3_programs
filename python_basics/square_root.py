#!/usr/local/bin/python3

from math import sqrt

number_str = input("What number do you want the square root for? ")

try:
	number = float(number_str)
except:
	print("That is not a valid number!")
	exit
	
print("The square root of {} is {}".format(number, sqrt(number)))