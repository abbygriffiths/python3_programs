#!/usr/local/bin/python3
from os import system
shopping_list = []

def help():
  print("Enter item name to add to list, or enter 'DONE' to exit program.")
  print("Enter 'HELP' to show this help message ")
  
while True:
  new_item = input("-->")
  if new_item == 'DONE':
    break

  shopping_list.append(new_item)
  print("Added {}! Your list contains {} items.".format(new_item, len(shopping_list)))

system('clear')
print("Your shopping list is: ")
for item in shopping_list:
  print("• {}".format(item))