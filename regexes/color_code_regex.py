#!/usr/local/bin/python3

import re
color_code_regex = r'/^\s*#?([A-F0-9]{6}|A-F0-9]{3})\s*$/i'

color_array = ['#123abc', 'abc123']
for color in color_array:
	match = re.match(color_code_regex, color)
	if match:
		print(match)
	else:
		print("{} did not match".format(color))
		
