#!/usr/bin/env/python3

from character import Character
from monster import Dragon
from monster import Goblin
from monster import Troll


class Game:
	def setup(self):
		self.player = Character()
		self.monsters = [
			Goblin(),
			Troll(),
			Dragon()
		]
		self.monster = self.get_next_monster()
		
		
	def get_next_monster(self):
		try:
			return self.monsters.pop(0)
		except IndexError:
			return None
		
		
	def monster_turn(self):
		#if self.monster.attack():
			#Inform player
			#if player.dodge():
				#continue
			#else
				#player.hit_points -= 1
		#else:
			#Inform player
		pass
	

	def player_turn(self):
		#Let player attack, rest, or quit
		#if attack():
			#self.player.attack()
			#if self.monster.dodge()
				#print("Monster dodged your attack!")
			#else:
				#self.monster.hit_points -= right_amount
		#elif rest():
			#self.player.rest()
		#elif quit():
			#Exit the game
		#else:
			#self.player_turn()
		pass


	def cleanup(self):
		#if self.monster.hit_points <= 0:
			#self.player.experience += self.monster.experience
			#self.monster = self.get_next_monster():
			#if self.monster:
				#self.monster_turn()
			#else:
				#Player wins
		pass