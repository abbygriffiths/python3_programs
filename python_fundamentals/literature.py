#!/usr/local/bin/python3
'''Prints the words of an UTF-8 text document at the URL provided through the command line.
'''
from urllib.request import urlopen
from sys import argv

def fetch_words(url):
	'''Fetches a list of words from a URL, which is provided by a single argument as a str object
	
	Args:
		url: The URL from which to fetch the words
		
	Returns:
		A list of strings containing the words
	
	'''
	with urlopen(url) as story:
		story_words = []
		for line in story:
			line_words = line.decode('utf-8').split()
			for word in line_words:
				story_words.append(word)
	return story_words


def print_items(items):
	'''Prints each element in a new line
	
	Args:
		items: An iterable series of printable items
	'''
	for item in items:
		print(item)


def main(url):
	'''Starting point of execution. Prints the words of a UTF-8 document
	from the Internet.
	
	Args:
		url: The URL to an UTF-8 encoded text document
	'''
	words = fetch_words(url)
	print_items(words)

if __name__ == '__main__':	
	main(argv[1])