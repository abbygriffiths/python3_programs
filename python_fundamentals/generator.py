#!/usr/local/bin/python3

def take(count, iterable):
	counter = 0
	
	for item in iterable:
		if counter == count:
			return
		else:
			counter += 1
			yield counter
		
		
def distinct(iterable):
	given = set()
	
	for item in iterable:
		if item in given:
			continue
		else:
			given.add(item)
			yield item
			
			
def run():
	iterable = [1, 2, 3, 4, 5, 2, 1, 6]
	items = []
	
	for item in take(7, distinct(iterable)):
		print(item)
		
		
if __name__ == '__main__':
	run()