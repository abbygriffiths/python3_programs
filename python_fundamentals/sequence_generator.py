#!/usr/local/bin/python3


def lucas(count):
    print("Lucas Sequence:")
    yield 2
    a = 2
    b = 1

    for i in range(0, count - 1):
        yield a
        a, b = b, a + b


def fibonacci(count):
    print("Fibonacci Sequence:")
    
    a = 0
    b = 1

    for i in range(0, count):
        yield a
        a, b = b, a + b


fibonacci_list = []
lucas_list = []

def run_sequences():
    for i in lucas(10):
        lucas_list.append(i)
        print(i)
    
    for i in fibonacci(10):
        fibonacci_list.append(i)
        print(i)

if __name__ == '__main__':
    run_sequences()